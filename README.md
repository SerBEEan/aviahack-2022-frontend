# Авиахакатон 2022 Frontend для Шереметьево

## Запуск
```
npm start
```
[Открыть сайт](http://localhost:3000/ "Открыть сайт")

## Docker

Собрать образ

```
docker build -t front ./
```

Запустить контейнер
```
docker run --rm -d -p 3000:3000 front
```

