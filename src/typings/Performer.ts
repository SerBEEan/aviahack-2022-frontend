import { Vehicle } from './Vehicle';

export interface Performer {
    taskPerformerId: string;
    taskStatus: string;
    driverName: string;
    driverId: string;
    vehicle: Vehicle;
}
