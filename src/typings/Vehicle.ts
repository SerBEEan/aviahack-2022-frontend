export interface Vehicle {
    id: string;
    name: string;
    capacity: number;
    averageSpeed: number;
}
