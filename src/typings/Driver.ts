import { User } from './User';
import { Vehicle } from './Vehicle';

export interface Driver {
    id: string;
    driver: User;
    vehicle: Vehicle;
}
