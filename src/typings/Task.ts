import { Performer } from './Performer';

export interface ShortTask {
    taskId: string;
    creationDate: string;
    startDate: string;
    estimationTime: string;
    flightNumber: string;
}

export type ShortTaskView = ShortTask;

export interface FullTask {
    taskId: string;
    creationDate: string;
    startDate: string;
    estimationTime: string;
    flightNumber: string;
    roadId: string;
    sourcePoint: string;
    targetPoint: string;
    distance: number;
    taskPerformerList: Performer[];
}
