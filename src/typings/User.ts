export interface UserCookie {
    token: string;
    login: string;
}

export interface LoginData {
    login: string;
    password: string;
}

export interface LoginResponse {
    token: string;
}

export enum RolePriority {
    Driver,
    Dispatcher,
    Manager,
}

interface UserRole {
    name: string;
    caption: string;
    priority: RolePriority;
}

export interface User {
    id: string;
    login: string;
    role: UserRole;
    name: string;
    active: boolean;
}
