import { Navigate } from 'react-router-dom';
import { Input, Form, Button, Spin } from 'antd';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { PATHS } from '../../constants/paths';
import { useAuth } from '../../hooks/useAuth';

import styles from './styles.module.css';

const { Item } = Form;

type LoginForm = {
    username: string;
    password: string;
};

const INIT_FORM: LoginForm = {
    username: '',
    password: '',
};

function LoginPage() {
    const { isAuth, isSending, auth } = useAuth();

    const onFinish = ({ password, username }: LoginForm) => {
        auth({ login: username, password });
    };

    return (
        <>
            {isAuth && <Navigate to={PATHS.home} />}
            <div className={styles.formContainer}>
                <Spin spinning={isSending} size="large">
                    <Form className={styles.form} size="large" initialValues={INIT_FORM} onFinish={onFinish}>
                        <Item name="username" rules={[{ required: true, message: 'Поле должно быть заполненным' }]}>
                            <Input prefix={<UserOutlined />} placeholder="Логин" />
                        </Item>
                        <Item name="password" rules={[{ required: true, message: 'Поле должно быть заполненным' }]}>
                            <Input.Password prefix={<LockOutlined />} type="password" placeholder="Пароль" />
                        </Item>
                        <Item>
                            <Button type="primary" htmlType="submit" block>
                                Оправить
                            </Button>
                        </Item>
                    </Form>
                </Spin>
            </div>
        </>
    );
}

export default LoginPage;
