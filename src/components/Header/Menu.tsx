import { Avatar, Dropdown, Menu as AntdMenu, Space } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';

import styles from './styles.module.css';

interface MenuProps {
    username: string;
    handleLogout: () => void;
}

function Menu({ username, handleLogout }: MenuProps) {
    return (
        <Dropdown
            overlay={
                <AntdMenu
                    items={[
                        {
                            key: '1',
                            danger: true,
                            label: 'Выйти',
                            onClick: handleLogout,
                        },
                    ]}
                />
            }
        >
            <Space className={styles.menuActions}>
                <Avatar size="large" icon={<UserOutlined />} />
                <Space className={styles.username}>
                    {username}
                    <DownOutlined />
                </Space>
            </Space>
        </Dropdown>
    );
}

export default Menu;
