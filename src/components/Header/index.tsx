import { Layout as AntdLayout } from 'antd';
import Menu from './Menu';
import { useAuth } from '../../hooks/useAuth';
import { useUser } from '../../hooks/useUser';

import styles from './styles.module.css';

const { Header: AntdHeader } = AntdLayout;

function Header() {
    const { isAuth, logout } = useAuth();
    const { user } = useUser();

    const handleLogout = () => {
        logout();
    };

    return (
        <AntdHeader className={styles.header}>
            <img className={styles.logo} src="./img/logo2.png" alt="Логотип 'Шереметьево. Международный аэропорт'" />
            {isAuth && <Menu username={user?.name ?? ''} handleLogout={handleLogout} />}
        </AntdHeader>
    );
}

export default Header;
