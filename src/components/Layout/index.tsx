import { PropsWithChildren } from 'react';
import { Layout as AntdLayout } from 'antd';
import Header from '../Header';

import styles from './styles.module.css';

const { Content } = AntdLayout;

function Layout({ children }: PropsWithChildren<unknown>) {
    return (
        <AntdLayout>
            <Header />
            <Content className={styles.content}>{children}</Content>
        </AntdLayout>
    );
}

export default Layout;
