import { useEffect, useState } from 'react';
import { Progress, ProgressProps } from 'antd';
import { calculateProgressByDates } from '../../utils/calculateProgressByDates';

interface ProgressWithDatesProps extends ProgressProps {
    startDate: Date;
    endDate: Date;
    externalProgress?: number;
}

function ProgressWithDates(props: ProgressWithDatesProps) {
    const { startDate, endDate, externalProgress, ...rest } = props;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [count, setCount] = useState(0);
    const progress = externalProgress ?? calculateProgressByDates(startDate, endDate, new Date());

    useEffect(() => {
        // Чтобы сделать изменение линии прогресса в реальном времени
        const timer = setInterval(() => {
            setCount(prev => (progress === 0 || progress >= 100 ? prev : prev + 1));
        }, 60000);

        return () => {
            clearInterval(timer);
        };
    }, [progress]);

    return <Progress {...rest} showInfo={false} status={progress >= 100 ? 'success' : 'active'} percent={progress} />;
}

export default ProgressWithDates;
