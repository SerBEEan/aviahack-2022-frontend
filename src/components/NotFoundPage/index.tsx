import { Result, Button } from 'antd';
import { Link } from 'react-router-dom';
import { PATHS } from '../../constants';

function NotFoundPage() {
    return (
        <Result
            status="404"
            title="404"
            subTitle="Извините, но такой страницы не существует."
            extra={
                <Link to={PATHS.home}>
                    <Button type="link">Вернуться на главную</Button>
                </Link>
            }
        />
    );
}

export default NotFoundPage;
