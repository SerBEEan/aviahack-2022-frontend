import { useEffect } from 'react';
import { Spin } from 'antd';
import DispatcherScreen from '../DispatcherScreen';
import DriverScreen from '../DriverScreen';
import ManagerScreen from '../ManagerScreen';
import { withAuthorization } from '../../hocs/withAuthorization';
import { useUser } from '../../hooks/useUser';

import styles from './styles.module.css';

function HomePage() {
    const { isDispatcher, isDriver, isManager, getUserInfo, login, isLoading } = useUser();

    useEffect(() => {
        if (login !== null) {
            getUserInfo(login);
        }
    }, [getUserInfo, login]);

    return (
        <>
            {isLoading ? (
                <div className={styles.blockCentering}>
                    <Spin size="large" spinning />
                </div>
            ) : (
                <>
                    {isDispatcher && <DispatcherScreen />}
                    {isDriver && <DriverScreen />}
                    {isManager && <ManagerScreen />}
                </>
            )}
        </>
    );
}

export default withAuthorization(HomePage);
