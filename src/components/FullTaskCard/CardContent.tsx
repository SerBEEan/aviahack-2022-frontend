import { useState, useEffect } from 'react';
import moment, { Moment } from 'moment';
import { Divider, Card, Form, DatePicker } from 'antd';
import { FullTask } from '../../typings/Task';
import Actions from './Actions';
import PerformerList from './PerformerList';
import ProgressWithDates from '../ProgressWithDates';
import { formatDate } from '../../utils/formatDate';
import { calculateProgressByDates } from '../../utils/calculateProgressByDates';
import { FORMAT_DATE } from '../../constants';

import styles from './styles.module.css';

const { Item, useForm } = Form;

interface CardContentProps {
    task: FullTask;
}

type FullTaskForm = {
    startDate: Moment;
    estimationTime: Moment;
};

function CardContent({ task }: CardContentProps) {
    const [isEdit, setIsEdit] = useState(false);
    const [form] = useForm<FullTaskForm>();

    const progress = calculateProgressByDates(new Date(task.startDate), new Date(task.estimationTime), new Date());

    const handleEditMode = () => {
        setIsEdit(true);
    };

    const handleReset = () => {
        form.resetFields();
        setIsEdit(false);
    };

    const handleSave = ({ estimationTime, startDate }: FullTaskForm) => {
        console.log(estimationTime.format());
        console.log(startDate.format());
        setIsEdit(false);
    };

    useEffect(() => {
        form.setFields([
            { name: 'startDate', value: moment(task.startDate) },
            { name: 'estimationTime', value: moment(task.estimationTime) },
        ]);
    }, [form, task]);

    return (
        <Form
            className={styles.form}
            form={form}
            initialValues={{ startDate: moment(task.startDate), estimationTime: moment(task.estimationTime) }}
            onFinish={handleSave}
            onReset={handleReset}
        >
            <Card
                className={styles.cardContend}
                title={`Рейс «${task.flightNumber}»`}
                extra={<Actions isEdit={isEdit} handleEditMode={handleEditMode} handleReset={handleReset} />}
            >
                <div className={styles.content}>
                    <div>Дата создания</div>
                    <div>{formatDate(task.creationDate, 'dateTime')}</div>
                    <div>Дата начала</div>
                    <div>
                        {isEdit ? (
                            <Item
                                name="startDate"
                                rules={[{ required: true, message: 'Поле должно быть заполненным' }]}
                            >
                                <DatePicker format={FORMAT_DATE.dateTime} showTime />
                            </Item>
                        ) : (
                            formatDate(task.startDate, 'dateTime')
                        )}
                    </div>
                    <div>Дата завершения</div>
                    <div>
                        {isEdit ? (
                            <Item
                                name="estimationTime"
                                rules={[{ required: true, message: 'Поле должно быть заполненным' }]}
                            >
                                <DatePicker format={FORMAT_DATE.dateTime} showTime />
                            </Item>
                        ) : (
                            formatDate(task.estimationTime, 'dateTime')
                        )}
                    </div>
                </div>
                <Divider />
                <ProgressWithDates
                    startDate={new Date(task.startDate)}
                    endDate={new Date(task.estimationTime)}
                    externalProgress={progress}
                />
                <Divider />
                <div className={styles.content}>
                    <div>Откуда</div>
                    <div>{task.sourcePoint}</div>
                    <div>Куда</div>
                    <div>{task.sourcePoint}</div>
                    <div>Дистанция</div>
                    <div>{`${task.distance} м`}</div>
                </div>
                <Divider />
                <PerformerList performers={task.taskPerformerList} taskId={task.taskId} />
            </Card>
        </Form>
    );
}

export default CardContent;
