import classnames from 'classnames';
import { Spin, Empty } from 'antd';
import CardContent from './CardContent';
import { useFullTask } from '../../hooks/useFullTask';

import styles from './styles.module.css';

function FullTaskCard() {
    const { isLoading, fullTask } = useFullTask();

    const isNotExistTask = fullTask === undefined;

    return (
        <div className={classnames({ [styles.centeredData]: isLoading || isNotExistTask })}>
            {isLoading ? (
                <div className={styles.spinnerContainer}>
                    <Spin spinning size="large" />
                </div>
            ) : isNotExistTask ? (
                <div>
                    <Empty description="Задача не выбрана" />
                </div>
            ) : (
                <CardContent task={fullTask} />
            )}
        </div>
    );
}

export default FullTaskCard;
