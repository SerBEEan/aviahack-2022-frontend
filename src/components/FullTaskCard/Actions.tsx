import { Button, Form } from 'antd';
import { EditOutlined } from '@ant-design/icons';

import styles from './styles.module.css';

const { Item } = Form;

interface ActionsProps {
    isEdit: boolean;
    handleEditMode: () => void;
    handleReset: () => void;
}

function Actions(props: ActionsProps) {
    const { isEdit, handleEditMode, handleReset } = props;

    return isEdit ? (
        <Item>
            <div className={styles.cardActions}>
                <Button type="primary" htmlType="submit" block>
                    Оправить
                </Button>
                <Button danger onClick={handleReset}>
                    Отменить
                </Button>
            </div>
        </Item>
    ) : (
        <Button icon={<EditOutlined />} onClick={handleEditMode}>
            Редактировать
        </Button>
    );
}

export default Actions;
