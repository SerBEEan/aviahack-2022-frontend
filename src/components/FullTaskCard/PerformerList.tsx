import { Select } from 'antd';
import { useDriver } from '../../hooks/useDriver';
import { useFullTask } from '../../hooks/useFullTask';
import { Performer } from '../../typings/Performer';

const { Option } = Select;

interface PerformerListProps {
    performers: Performer[];
    taskId: string;
}

function PerformerList({ performers, taskId }: PerformerListProps) {
    const { drivers, isLoading } = useDriver();
    const { addPerformer, deletePerformer } = useFullTask();

    const selectPerformer = (driverId: string) => {
        addPerformer({ taskId, driverId });
    };

    const removePerformer = (driverId: string) => {
        deletePerformer({ taskId, driverId });
    };

    return (
        <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Выберите водителя"
            defaultValue={performers.map(performer => performer.driverId)}
            onSelect={selectPerformer}
            onDeselect={removePerformer}
            loading={isLoading}
        >
            {drivers.map(driver => (
                <Option key={driver.id}>{driver.driver.name}</Option>
            ))}
        </Select>
    );
}

export default PerformerList;
