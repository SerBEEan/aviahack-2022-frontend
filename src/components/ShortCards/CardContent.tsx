import { ShortTaskView } from '../../typings/Task';
import ProgressWithDates from '../ProgressWithDates';
import { formatDate } from '../../utils/formatDate';

import styles from './styles.module.css';

interface CardContentProps {
    task: ShortTaskView;
}

function CardContent({ task }: CardContentProps) {
    return (
        <>
            <div className={styles.contentContainer}>
                <span>Дата создания</span>
                <span className={styles.contentData}>{formatDate(task.creationDate, 'dateTime')}</span>
                <span>Дата начала</span>
                <span className={styles.contentData}>{formatDate(task.startDate, 'dateTime')}</span>
                <span>Дата завершения</span>
                <span className={styles.contentData}>{formatDate(task.estimationTime, 'dateTime')}</span>
            </div>
            <ProgressWithDates startDate={new Date(task.startDate)} endDate={new Date(task.estimationTime)} />
        </>
    );
}

export default CardContent;
