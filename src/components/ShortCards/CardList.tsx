import InfiniteScroll from 'react-infinite-scroll-component';
import { List, Spin, Card, Button } from 'antd';
import { CheckOutlined } from '@ant-design/icons';
import CardContent from './CardContent';
import { useShortTasks } from '../../hooks/useShortTasks';
import { useFullTask } from '../../hooks/useFullTask';

import styles from './styles.module.css';

interface CardListProps {
    driverId?: string;
}

function CardList({ driverId }: CardListProps) {
    const { tasks, getTasks, isLoading } = useShortTasks();
    const { selectTaskById } = useFullTask();

    const isDriverExist = driverId !== undefined;

    const loadMoreTasks = () => {
        if (isLoading) {
            return;
        }

        getTasks();
    };

    const clickTaskCard = (taskId: string) => {
        selectTaskById(taskId);
    };

    return (
        <div className={styles.listContainer} id="scrollableDiv">
            <InfiniteScroll
                dataLength={tasks.length}
                next={loadMoreTasks}
                hasMore={tasks.length < 50}
                loader={<div className={styles.moreRequestSpin}>{tasks.length !== 0 && <Spin />}</div>}
                endMessage={<>Больше задач нет</>}
                scrollableTarget="scrollableDiv"
            >
                <List
                    loading={isLoading && tasks.length === 0}
                    dataSource={tasks}
                    renderItem={task => (
                        <List.Item key={task.taskId} className={styles.listItem}>
                            <List.Item.Meta
                                title={task.flightNumber}
                                description={
                                    <Card
                                        className={styles.taskCard}
                                        hoverable={!isDriverExist}
                                        onClick={isDriverExist ? undefined : () => clickTaskCard(task.taskId)}
                                        actions={
                                            isDriverExist
                                                ? [
                                                      <Button
                                                          key={1}
                                                          className={styles.actionSuccess}
                                                          size="large"
                                                          type="primary"
                                                          icon={<CheckOutlined />}
                                                      >
                                                          Принять
                                                      </Button>,
                                                  ]
                                                : undefined
                                        }
                                    >
                                        <CardContent task={task} />
                                    </Card>
                                }
                            />
                        </List.Item>
                    )}
                />
            </InfiniteScroll>
        </div>
    );
}

export default CardList;
