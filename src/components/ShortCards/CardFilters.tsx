import { Checkbox, DatePicker, Slider } from 'antd';
import type { SliderMarks } from 'antd/es/slider';

import styles from './styles.module.css';

let hourCount = 0;
const MIN = 0;
const MAX = 49;

const TIMES = Array(MAX)
    .fill(0)
    .map((_, i) => {
        const prepareTime = ['', ''];

        prepareTime[0] = String(hourCount);

        if (i % 2 !== 0) {
            prepareTime[1] = '30';
            hourCount++;
        } else {
            prepareTime[1] = '00';
        }

        return prepareTime.join(':');
    });

const MASK: SliderMarks = {
    [MIN]: TIMES[0],
    [MAX - 1]: TIMES[TIMES.length - 1],
};

const formatter = (value?: number) => (value === undefined ? <></> : TIMES[value]);

function CardFilters() {
    return (
        <div>
            <div className={styles.filterControls}>
                <div>
                    <DatePicker className={styles.picker} />
                </div>
                <div>
                    <Checkbox>Показать только активные</Checkbox>
                </div>
            </div>

            <Slider
                tooltip={{ formatter }}
                range
                defaultValue={[MIN, MAX]}
                marks={MASK}
                min={MIN}
                max={MAX - 1}
                step={1}
            />
        </div>
    );
}

export default CardFilters;
