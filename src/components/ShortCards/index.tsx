import { Divider } from 'antd';
import CardFilters from './CardFilters';
import CardList from './CardList';

import styles from './styles.module.css';

function ShortCardList() {
    return (
        <div className={styles.shortCardListContainer}>
            <CardFilters />
            <Divider />
            <CardList />
        </div>
    );
}

export default ShortCardList;
