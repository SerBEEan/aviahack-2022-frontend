import CardList from '../ShortCards/CardList';
import { useUser } from '../../hooks/useUser';

import styles from './styles.module.css';

function DriverScreen() {
    const { user } = useUser();

    return (
        <div className={styles.driverScreenContainer}>
            <CardList driverId={user?.id} />
        </div>
    );
}

export default DriverScreen;
