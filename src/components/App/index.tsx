import { Routes, Route } from 'react-router-dom';
import Layout from '../Layout';
import HomePage from '../HomePage';
import LoginPage from '../LoginPage';
import NotFoundPage from '../NotFoundPage';
import { PATHS } from '../../constants';

function App() {
    return (
        <Layout>
            <Routes>
                <Route path={PATHS.home} element={<HomePage />} />
                <Route path={PATHS.login} element={<LoginPage />} />
                <Route path="*" element={<NotFoundPage />} />
            </Routes>
        </Layout>
    );
}

export default App;
