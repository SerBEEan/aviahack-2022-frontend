import ShortCards from '../ShortCards';
import FullTaskCard from '../FullTaskCard';

import styles from './styles.module.css';

function DispatcherScreen() {
    return (
        <div className={styles.dispatcherScreen}>
            <ShortCards />
            <FullTaskCard />
        </div>
    );
}

export default DispatcherScreen;
