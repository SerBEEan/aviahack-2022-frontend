import { ComponentType } from 'react';
import { Navigate } from 'react-router-dom';
import { PATHS } from '../constants';
import { useAuth } from '../hooks/useAuth';

export function withAuthorization<T extends JSX.IntrinsicAttributes>(Component: ComponentType<T>) {
    function Authorization(props: T) {
        const { isAuth } = useAuth();
        return isAuth ? <Component {...props} /> : <Navigate to={PATHS.login} />;
    }

    return Authorization;
}
