import { createSlice, createEntityAdapter, createAsyncThunk } from '@reduxjs/toolkit';
import Cookies from 'js-cookie';
import { Api } from '../api';
import { UserParams } from '../api/User';
import { COOKIES_SELECTORS } from '../constants';
import { getCookieData, setCookieData } from '../utils/getCookieData';
import { RootState } from './index';
import { LoginData, User, UserCookie } from '../typings/User';

const userAdapter = createEntityAdapter<User>();

const userCookie = getCookieData<UserCookie>('token');
const isAuth = userCookie !== undefined;
const login = userCookie?.login ?? null;

const initialState = {
    user: userAdapter.getInitialState({ isSending: false, isLoading: false }),
    isAuth,
    login,
};

export const authUser = createAsyncThunk('user/authUser', async (params: LoginData) => {
    const data = await Api.user.authUser(params);

    if (data === undefined) {
        throw new Error('No success');
    }

    setCookieData('token', { token: data.token, login: params.login });
    return params.login;
});

export const getUser = createAsyncThunk('user/getUser', async ({ login }: UserParams) => {
    const data = await Api.user.getUser({ login });

    if (!data) {
        throw new Error('No success');
    }

    return data;
});

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        logoutUser(state) {
            state.isAuth = false;
            Cookies.remove(COOKIES_SELECTORS.token);
        },
    },
    extraReducers: builder => {
        builder.addCase(authUser.pending, state => {
            state.user.isSending = true;
        });
        builder.addCase(authUser.fulfilled, (state, action) => {
            state.user.isSending = false;
            state.login = action.payload;
            state.isAuth = true;
        });
        builder.addCase(authUser.rejected, state => {
            state.user.isSending = false;
        });

        builder.addCase(getUser.pending, state => {
            state.user.isLoading = true;
        });
        builder.addCase(getUser.fulfilled, (state, action) => {
            state.user.isLoading = false;
            userAdapter.setAll(state.user, [action.payload]);
        });
        builder.addCase(getUser.rejected, state => {
            state.user.isLoading = false;
        });
    },
});

export const userActions = userSlice.actions;

export const userSelectors = {
    selectIsAuth: (state: RootState) => {
        return state.userState.isAuth;
    },
    selectIsLoading: (state: RootState) => {
        return state.userState.user.isLoading;
    },
    selectIsSending: (state: RootState) => {
        return state.userState.user.isSending;
    },
    selectLogin: (state: RootState) => {
        return state.userState.login;
    },
    ...userAdapter.getSelectors((state: RootState) => state.userState.user),
};

export default userSlice.reducer;
