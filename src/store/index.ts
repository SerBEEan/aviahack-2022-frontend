import { configureStore } from '@reduxjs/toolkit';
import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';
import userSlice from './userSlice';
import taskSlice from './taskSlice';
import driverSlice from './driverSlice';

export const store = configureStore({
    reducer: {
        userState: userSlice,
        taskState: taskSlice,
        driverState: driverSlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
