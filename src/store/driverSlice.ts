import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { Api } from '../api';
import { RootState } from './index';
import { Driver } from '../typings/Driver';

const allDriversAdapter = createEntityAdapter<Driver>();

const initialState = {
    allDrivers: allDriversAdapter.getInitialState({ loading: false }),
};

export const getAllDrivers = createAsyncThunk('driver/getAllDrivers', async () => {
    const data = await Api.driver.getAllDrivers();

    if (!data) {
        throw new Error('No success');
    }

    return data;
});

const driverSlice = createSlice({
    name: 'driver',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(getAllDrivers.pending, state => {
            state.allDrivers.loading = true;
        });
        builder.addCase(getAllDrivers.fulfilled, (state, action) => {
            state.allDrivers.loading = false;
            allDriversAdapter.setAll(state.allDrivers, action.payload);
        });
        builder.addCase(getAllDrivers.rejected, state => {
            state.allDrivers.loading = false;
        });
    },
});

export const allDriversSelectors = {
    selectLoading: (state: RootState) => {
        return state.taskState.shortTasks.loading;
    },
    ...allDriversAdapter.getSelectors((state: RootState) => state.driverState.allDrivers),
};

export default driverSlice.reducer;
