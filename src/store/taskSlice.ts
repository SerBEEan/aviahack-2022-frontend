import { createAsyncThunk, createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Api } from '../api';
import { ByTaskIdParams, PerformerInTaskParams } from '../api/Task';
import { RootState } from './index';
import { ShortTaskView, FullTask } from '../typings/Task';

export type { PerformerInTaskParams } from '../api/Task';

const shortTasksAdapter = createEntityAdapter<ShortTaskView>({
    selectId: task => task.taskId,
});
const fullTasksByIdAdapter = createEntityAdapter<FullTask>({
    selectId: task => task.taskId,
});

const initialState = {
    shortTasks: shortTasksAdapter.getInitialState({ loading: false }),
    fullTasksById: fullTasksByIdAdapter.getInitialState({ loading: false, sending: false }),
    selectedTaskId: null as string | null,
};

export const getShortTasks = createAsyncThunk('task/getShortTasks', async () => {
    const data = await Api.task.getShortTasks();

    if (!data) {
        throw new Error('No success');
    }

    return data;
});

export const getFullTaskById = createAsyncThunk('task/getFullTaskById', async (params: ByTaskIdParams) => {
    const data = await Api.task.getFullTaskById(params);

    if (!data) {
        throw new Error('No success');
    }

    return data;
});

export const addPerformerToTask = createAsyncThunk('task/addPerformerToTask', async (params: PerformerInTaskParams) => {
    Api.task.addPerformerToTask(params);
});

export const deletePerformerToTask = createAsyncThunk(
    'task/deletePerformerToTask',
    async (params: PerformerInTaskParams) => {
        Api.task.deletePerformerFromTask(params);
    }
);

const taskSlice = createSlice({
    name: 'task',
    initialState,
    reducers: {
        changeSelectedId(state, action: PayloadAction<string>) {
            state.selectedTaskId = action.payload;
        },
    },
    extraReducers: builder => {
        builder.addCase(getShortTasks.pending, state => {
            state.shortTasks.loading = true;
        });
        builder.addCase(getShortTasks.fulfilled, (state, action) => {
            state.shortTasks.loading = false;
            shortTasksAdapter.addMany(state.shortTasks, action.payload);
        });
        builder.addCase(getShortTasks.rejected, state => {
            state.shortTasks.loading = false;
        });

        builder.addCase(getFullTaskById.pending, state => {
            state.fullTasksById.loading = true;
        });
        builder.addCase(getFullTaskById.fulfilled, (state, action) => {
            state.fullTasksById.loading = false;
            fullTasksByIdAdapter.addOne(state.fullTasksById, action.payload);
            state.selectedTaskId = action.payload.taskId;
        });
        builder.addCase(getFullTaskById.rejected, state => {
            state.fullTasksById.loading = false;
        });

        builder.addCase(addPerformerToTask.pending, state => {
            state.fullTasksById.sending = true;
        });
        builder.addCase(addPerformerToTask.fulfilled, state => {
            state.fullTasksById.sending = false;
        });
        builder.addCase(addPerformerToTask.rejected, state => {
            state.fullTasksById.sending = false;
        });

        builder.addCase(deletePerformerToTask.pending, state => {
            state.fullTasksById.sending = true;
        });
        builder.addCase(deletePerformerToTask.fulfilled, state => {
            state.fullTasksById.sending = false;
        });
        builder.addCase(deletePerformerToTask.rejected, state => {
            state.fullTasksById.sending = false;
        });
    },
});

export const tasksActions = taskSlice.actions;

export const tasksSelectors = {
    selectLoading: (state: RootState) => {
        return state.taskState.shortTasks.loading;
    },
    ...shortTasksAdapter.getSelectors((state: RootState) => state.taskState.shortTasks),
};

export const taskByIdSelectors = {
    selectLoading: (state: RootState) => {
        return state.taskState.fullTasksById.loading;
    },
    selectSending: (state: RootState) => {
        return state.taskState.fullTasksById.sending;
    },
    selectSelectedTaskId: (state: RootState) => {
        return state.taskState.selectedTaskId;
    },
    ...fullTasksByIdAdapter.getSelectors((state: RootState) => state.taskState.fullTasksById),
};

export default taskSlice.reducer;
