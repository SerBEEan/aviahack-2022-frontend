import { useCallback } from 'react';
import { useAppSelector, useAppDispatch } from '../store';
import { getUser, userSelectors } from '../store/userSlice';
import { User, RolePriority } from '../typings/User';

type GetUserInfo = (login: string) => void;

type Return = {
    login: string | null;
    isDriver: boolean;
    isDispatcher: boolean;
    isManager: boolean;
    isLoading: boolean;
    user: User | undefined;
    getUserInfo: GetUserInfo;
};

export function useUser(): Return {
    const dispatch = useAppDispatch();

    const isLoading = useAppSelector(userSelectors.selectIsLoading);
    const user = useAppSelector(userSelectors.selectAll)[0] as User | undefined;
    const login = useAppSelector(userSelectors.selectLogin);

    const isDriver = user?.role.priority === RolePriority.Driver;
    const isDispatcher = (user && (user.role.priority === RolePriority.Dispatcher || user.role.priority > 2)) ?? false;
    const isManager = user?.role.priority === RolePriority.Manager;

    const getUserInfo = useCallback<GetUserInfo>(
        login => {
            dispatch(getUser({ login }));
        },
        [dispatch]
    );

    return {
        login,
        isLoading,
        user,
        getUserInfo,
        isDriver,
        isDispatcher,
        isManager,
    };
}
