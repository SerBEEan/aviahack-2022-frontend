import { useCallback, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../store';
import {
    getFullTaskById,
    taskByIdSelectors,
    tasksActions,
    addPerformerToTask,
    deletePerformerToTask,
    PerformerInTaskParams,
} from '../store/taskSlice';
import { FullTask } from '../typings/Task';

type SelectTaskById = (id: string) => void;
type AddPerformer = (params: PerformerInTaskParams) => void;
type DeletePerformer = (params: PerformerInTaskParams) => void;

type Return = {
    isLoading: boolean;
    isSending: boolean;
    fullTask: FullTask | undefined;
    selectTaskById: SelectTaskById;
    addPerformer: AddPerformer;
    deletePerformer: DeletePerformer;
};

export function useFullTask(): Return {
    const dispatch = useAppDispatch();

    const isLoading = useAppSelector(taskByIdSelectors.selectLoading);
    const isSending = useAppSelector(taskByIdSelectors.selectSending);
    const selectedId = useAppSelector(taskByIdSelectors.selectSelectedTaskId);
    const fullTask = useAppSelector(state => taskByIdSelectors.selectById(state, selectedId ?? -1));

    const selectTaskById = useCallback<SelectTaskById>(
        id => {
            dispatch(tasksActions.changeSelectedId(id));
        },
        [dispatch]
    );

    const addPerformer = useCallback<AddPerformer>(
        params => {
            dispatch(addPerformerToTask(params));
        },
        [dispatch]
    );

    const deletePerformer = useCallback<DeletePerformer>(
        params => {
            dispatch(deletePerformerToTask(params));
        },
        [dispatch]
    );

    useEffect(() => {
        if (selectedId !== null && fullTask === undefined) {
            dispatch(getFullTaskById({ id: selectedId }));
        }
    }, [dispatch, selectedId, fullTask]);

    return {
        isLoading,
        isSending,
        fullTask,
        selectTaskById,
        addPerformer,
        deletePerformer,
    };
}
