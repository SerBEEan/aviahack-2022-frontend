import { useCallback, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../store';
import { getAllDrivers, allDriversSelectors } from '../store/driverSlice';
import { Driver } from '../typings/Driver';

type Return = {
    drivers: Driver[];
    isLoading: boolean;
    handleGetAllDrivers: () => void;
};

export function useDriver(): Return {
    const dispatch = useAppDispatch();
    const drivers = useAppSelector(allDriversSelectors.selectAll);
    const isLoading = useAppSelector(allDriversSelectors.selectLoading);

    const handleGetAllDrivers = useCallback(() => {
        dispatch(getAllDrivers());
    }, [dispatch]);

    useEffect(() => {
        dispatch(getAllDrivers());
    }, [dispatch]);

    return {
        drivers,
        isLoading,
        handleGetAllDrivers,
    };
}
