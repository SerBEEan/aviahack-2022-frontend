import { useCallback } from 'react';
import { useAppSelector, useAppDispatch } from '../store';
import { authUser, userActions, userSelectors } from '../store/userSlice';
import { LoginData } from '../typings/User';

type Return = {
    isAuth: boolean;
    isSending: boolean;
    auth: (params: LoginData) => void;
    logout: () => void;
};

export function useAuth(): Return {
    const dispatch = useAppDispatch();

    const isAuth = useAppSelector(userSelectors.selectIsAuth);
    const isSending = useAppSelector(userSelectors.selectIsSending);

    const auth = useCallback(
        (params: LoginData) => {
            dispatch(authUser(params));
        },
        [dispatch]
    );

    const logout = useCallback(() => {
        dispatch(userActions.logoutUser());
    }, [dispatch]);

    return {
        isAuth,
        isSending,
        auth,
        logout,
    };
}
