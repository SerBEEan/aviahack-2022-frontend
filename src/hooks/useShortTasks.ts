import { useEffect, useCallback } from 'react';
import { useAppSelector, useAppDispatch } from '../store';
import { getShortTasks, tasksSelectors } from '../store/taskSlice';
import { ShortTaskView } from '../typings/Task';

type Return = {
    tasks: ShortTaskView[];
    isLoading: boolean;
    getTasks: () => void;
};

export function useShortTasks(): Return {
    const dispatch = useAppDispatch();

    const tasks = useAppSelector(tasksSelectors.selectAll);
    const isLoading = useAppSelector(tasksSelectors.selectLoading);

    const getTasks = useCallback(() => {
        dispatch(getShortTasks());
    }, [dispatch]);

    useEffect(() => {
        getTasks();
    }, [getTasks]);

    return {
        tasks,
        isLoading,
        getTasks,
    };
}
