import moment from 'moment';
import { FORMAT_DATE } from '../constants';

export function formatDate(date: string, format: keyof typeof FORMAT_DATE): string {
    return moment(date).format(FORMAT_DATE[format]);
}
