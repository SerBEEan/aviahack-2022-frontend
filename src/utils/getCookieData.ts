import Cookies from 'js-cookie';
import { COOKIES_SELECTORS } from '../constants/cookies';

export function getCookieData<T>(name: keyof typeof COOKIES_SELECTORS): T | undefined {
    const data = Cookies.get(name);

    return data === undefined ? undefined : JSON.parse(data);
}

export function setCookieData<T>(name: keyof typeof COOKIES_SELECTORS, data: T) {
    Cookies.set(name, JSON.stringify(data));
}
