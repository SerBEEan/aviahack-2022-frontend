export function calculateProgressByDates(start: Date, finish: Date, current: Date): number {
    const allRange = finish.getTime() - start.getTime();
    const currentRange = current.getTime() - start.getTime();

    return (currentRange * 100) / allRange;
}
