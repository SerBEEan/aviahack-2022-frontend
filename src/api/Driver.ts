import { get } from './request';
import { Driver } from '../typings/Driver';

export function getAllDrivers() {
    return get<Driver[]>({
        url: '/driver',
        errorMessage: 'Не удалось получить список всех водителей',
    });
}
