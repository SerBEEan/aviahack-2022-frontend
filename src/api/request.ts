import { notification } from 'antd';
import { store } from '../store';
import { userActions } from '../store/userSlice';
import { getCookieData } from '../utils/getCookieData';
import { UserCookie } from '../typings/User';

interface RequestParams {
    url: string;
    options?: RequestInit;
    errorMessage?: string;
    successMessage?: string;
}

async function request(url: string, options?: RequestInit): Promise<Response> {
    try {
        const token = getCookieData<UserCookie>('token')?.token;
        const headers: HeadersInit = {
            'Content-Type': 'application/json',
            ...options?.headers,
            ...(token !== undefined && { Authorization: `Bearer ${token}` }),
        };

        const response = await fetch(`${process.env.REACT_APP_API_BASE}${url}`, {
            ...options,
            headers,
        });

        // Если токен просрочен
        if (response.status === 403) {
            store.dispatch(userActions.logoutUser());
        }

        return response;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
    } catch (e: Error) {
        notification.error({ message: 'Неизвестная ошибка на сервере' });
        const msg = `${e.name}: ${e.message}`;
        console.error(msg);
        return new Response(undefined, { status: 520, statusText: msg });
    }
}

function notify(response: Response, errorMessage?: string, successMessage?: string) {
    if (response.ok && successMessage) {
        notification.success({ message: successMessage });
    }

    if (!response.ok && errorMessage) {
        notification.error({ message: errorMessage });
    }
}

export async function get<T = never>({
    url,
    options,
    errorMessage,
    successMessage,
}: RequestParams): Promise<T | undefined> {
    const response = await request(url, options);
    notify(response, errorMessage, successMessage);

    if (!response.ok) {
        return undefined;
    }

    const data = await response.text();
    return (data ? JSON.parse(data) : data) as T;
}

export async function post<T = never>({
    url,
    options,
    errorMessage,
    successMessage,
}: RequestParams): Promise<T | undefined> {
    return get<T>({ url, options: { ...options, method: 'POST' }, errorMessage, successMessage });
}

export async function put<T = never>({
    url,
    options,
    errorMessage,
    successMessage,
}: RequestParams): Promise<T | undefined> {
    return get<T>({ url, options: { ...options, method: 'PUT' }, errorMessage, successMessage });
}

export async function del<T = never>({
    url,
    options,
    errorMessage,
    successMessage,
}: RequestParams): Promise<T | undefined> {
    return get<T>({ url, options: { ...options, method: 'DELETE' }, errorMessage, successMessage });
}
