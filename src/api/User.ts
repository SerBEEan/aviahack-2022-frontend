import { post, get } from './request';
import { LoginData, LoginResponse, User } from '../typings/User';

export type UserParams = { login: string };

export function authUser(loginData: LoginData) {
    return post<LoginResponse>({
        url: '/user/login',
        options: {
            body: JSON.stringify(loginData),
        },
        errorMessage: 'Не удалось авторизоваться',
    });
}

export function getUser({ login }: UserParams) {
    return get<User>({
        url: `/user?login=${login}`,
        errorMessage: 'Не удалось информацию о пользователе',
    });
}
