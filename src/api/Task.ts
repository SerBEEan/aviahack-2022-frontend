import { get, post, put, del } from './request';
import { ShortTask, FullTask } from '../typings/Task';

export type ByTaskIdParams = { id: string };
export type UpdateTaskParams = { task: FullTask };
export type PerformerInTaskParams = { taskId: string; driverId: string };

export function getShortTasks() {
    return get<ShortTask[]>({
        url: '/task?allData=true',
        errorMessage: 'Не удалось получить список задач',
    });
}

export function getFullTaskById({ id }: ByTaskIdParams) {
    return get<FullTask>({
        url: `/task/${id}`,
        errorMessage: 'Не удалось получить подробную информацию о задаче',
    });
}

export function addPerformerToTask({ driverId, taskId }: PerformerInTaskParams) {
    return post<void>({
        url: `/task/performers/add`,
        options: {
            body: JSON.stringify({ driverId, taskId }),
        },
        errorMessage: 'Не удалось добавить исполнителя в задачу',
        successMessage: 'Исполнитель успешно добавлен',
    });
}

export function deletePerformerFromTask({ driverId, taskId }: PerformerInTaskParams) {
    return del<void>({
        url: `/task/performers/delete`,
        options: {
            body: JSON.stringify({ driverId, taskId }),
        },
        errorMessage: 'Не удалось удалить исполнителя из задачи',
        successMessage: 'Исполнитель успешно удален',
    });
}

export function updateFullTaskById({ task }: UpdateTaskParams) {
    return put<FullTask>({
        url: `/task`,
        options: {
            body: JSON.stringify(task),
        },
        errorMessage: 'Не удалось изменить задачу',
        successMessage: 'Задача успешно изменена',
    });
}
