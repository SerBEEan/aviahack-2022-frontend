import * as user from './User';
import * as task from './Task';
import * as driver from './Driver';

export const Api = {
    user,
    task,
    driver,
};
