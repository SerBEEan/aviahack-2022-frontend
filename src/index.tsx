import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConfigProvider } from 'antd';
import ru from 'antd/es/locale/ru_RU';
import App from './components/App';
import { store } from './store';
import 'moment/locale/ru';

import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <Provider store={store}>
        <BrowserRouter>
            <ConfigProvider locale={ru}>
                <App />
            </ConfigProvider>
        </BrowserRouter>
    </Provider>
);
