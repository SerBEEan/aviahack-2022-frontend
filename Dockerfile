FROM node:16
WORKDIR /usr/src/app
COPY . .
RUN npm ci
RUN npm install serve
RUN npm run build
EXPOSE 3000
CMD [ "./node_modules/.bin/serve", "-d", "build" ]